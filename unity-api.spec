%global debug_package %{nil}

%define soname  libunity-api
%define sover   0
%define _version r1091
Name:           unity-api
Version:        r1091
Release:        0
Summary:        API for Unity shell integration
License:        LGPL-3.0-only
Group:          System/Libraries
URL:            https://launchpad.net/unity-api
Source:         https://github.com/abhishek9650/unity-api/archive/%{name}-%{version}.tar.gz
# PATCH-FIX-OPENSUSE unity-api-fix-missing-header.patch  -- Fix missing "functional" header.
Patch1:			EnableSourceTreeBuild.patch
BuildRequires:  cmake
BuildRequires:  fdupes
BuildRequires:  lcov
BuildRequires:  pkgconfig
BuildRequires:  pkgconfig(glib-2.0)
%if 0%{?is_opensuse}
BuildRequires:  cppcheck
%endif
%if 0%{?suse_version} >= 1500
BuildRequires:  gcc-c++
%else
BuildRequires:  gcc-c++
%endif
%description
Library to integrate with the Unity shell.

%package -n %{soname}%{sover}
Summary:        API for Unity shell integration
Group:          System/Libraries

%description -n %{soname}%{sover}
Library to integrate with the Unity shell.

%package devel
Summary:        Development files of libunity-api
Group:          Development/Libraries/C and C++
Requires:       %{soname}%{sover} = %{version}

%description devel
The libunity-api development package includes the header files,
libraries, development tools necessary for compiling and linking
application which will use libunity-api.


%prep
%setup -q
%patch1 -p1


%build
%if 0%{?suse_version} < 1500
export CC="gcc"
export CXX="g++"
export CFLAGS="$(echo '%{optflags}' | sed 's/-fstack-clash-protection//')"
export CXXFLAGS="$CFLAGS -fabi-version=2 -fabi-compat-version=2"
%endif
%cmake \
  -DCMAKE_INSTALL_PREFIX=%{_prefix}       \
  -DCMAKE_LIBRARY_ARCHITECTURE=../%{_lib} \
  -DCMAKE_INSTALL_LIBDIR=%{_libdir}  \
  -DNO_TESTS=ON
make %{?_smp_mflags} V=1

%install
%make_install


%post -n %{soname}%{sover} -p /sbin/ldconfig

%postun -n %{soname}%{sover} -p /sbin/ldconfig

%files -n %{soname}%{sover}
%license COPYING
%{_libdir}/%{soname}.so
%{_libdir}/%{soname}.so.0
%{_libdir}/%{soname}.so.0.1

%files devel
%{_includedir}/unity/
%{_libdir}/pkgconfig/*.pc

%changelog

